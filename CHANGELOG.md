# Change Log
All notable changes to this project will be documented in this file.

#### 1.x Releases
- `2.6.x` Releases  - [2.6.0](#260) | [2.6.1](#261) | [2.6.2](#262) | [2.6.3](#263)  | [2.6.4](#264) | [2.6.5](#265) | [2.6.6](#266) | [2.6.7](#267)
- `2.5.x` Releases  - [2.5.0](#250)

---
## [2.6.7](https://github.com/congncif/IDMFoundation/releases/tag/2.6.7)
Released on 2018-5-5

#### Fixed
-  `Integration` cannot run with type `.only`

## [2.6.6](https://github.com/congncif/IDMFoundation/releases/tag/2.6.6)
Released on 2018-4-23

#### Updated
- Update `IntegrationCall` operators

## [2.6.4](https://github.com/congncif/IDMFoundation/releases/tag/2.6.4)
Released on 2018-4-19

#### Updated
- Progress protocol

## [2.6.3](https://github.com/congncif/IDMFoundation/releases/tag/2.6.3)
Released on 2018-4-19

#### Updated
- `IntegrationCall` nextSuccess, nextError : configuration with parameters
- `IntegrationCall`  added transformNext

## [2.6.2](https://github.com/congncif/IDMFoundation/releases/tag/2.6.2)
Released on 2018-4-17

#### Updated
- `IntegrationCall` nextSuccess, nextError with parametersBuilder

## [2.6.1](https://github.com/congncif/IDMFoundation/releases/tag/2.6.1)
Released on 2018-4-11

#### Updated
- IntegrationCall: Add onProgress to tracking

## [2.6.0](https://github.com/congncif/IDMFoundation/releases/tag/2.6.0)
Released on 2018-4-8

#### Updated
- Integrator now is sub-class of NSObject
- Added reference to get integrator from an integration call
- Update Integration Batch Call

## [2.5.0](https://github.com/congncif/IDMFoundation/releases/tag/2.5.0)
Released on 2018-4-3

#### Added
- Added IntegrationCall next operator
- Updated IntegrationBatchCall


## [1.0.0](https://github.com/congncif/IDMFoundation/releases/tag/1.0.0)

#### Added
- Initial release of IDMCore.
- Added by [NGUYEN CHI CONG](https://github.com/congncif).
